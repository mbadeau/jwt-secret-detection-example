# Example secrets detection

This repo gives an example on how to extend [Secrets Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/#configure-scan-settings) that uses [Gitleaks](https://github.com/gitleaks/gitleaks) under the hood.